package ui;

import dao.LoginDao;
import java.awt.Image;
import java.util.ArrayList;
import model.User;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Kanny
 */
public class Login extends javax.swing.JFrame {

    /**
     * Creates new form Login
     */
    private ArrayList<User> userList;
    private User user;
    private ArrayList<onLogin> subscribers = new ArrayList();
    
    public Login() {
        initComponents();
        LoginDao dao = new LoginDao();
        lblWrong.setVisible(false);
        userList = dao.getAll();
        try {
            File fileBg = new File("img/bg_green.PNG");
            BufferedImage imageBg = ImageIO.read(fileBg);
            lblBackground.setIcon(new ImageIcon(imageBg));
            
            File fileLogo = new File("img/logo_login.PNG");
            String pathLogo = "img/logo_login.PNG";
            BufferedImage imageLogo = ImageIO.read(fileLogo);
            lblLogo.setIcon(resizeImage(pathLogo));
            
            File fileUserIcon = new File("img/usericon.PNG");
            BufferedImage imageUserIcon = ImageIO.read(fileUserIcon);
            lblUserIcon.setIcon(new ImageIcon(imageUserIcon));
            
        } catch (IOException ex) {
            Logger.getLogger(ProductPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelBg = new javax.swing.JPanel();
        lblLogo = new javax.swing.JLabel();
        lblUserIcon = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        userLogin = new javax.swing.JTextField();
        passLogin = new javax.swing.JPasswordField();
        loginBtn = new javax.swing.JButton();
        lblWrong = new javax.swing.JLabel();
        lblUser = new javax.swing.JLabel();
        lblPass = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        lblBackground = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Login | Inthanin Coffee");
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panelBg.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        panelBg.add(lblLogo, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 250, 350, 150));

        lblUserIcon.setForeground(new java.awt.Color(255, 255, 255));
        lblUserIcon.setText("Icon");
        panelBg.add(lblUserIcon, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 50, 200, 200));

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        userLogin.setBackground(new java.awt.Color(0, 0, 0));
        userLogin.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        userLogin.setForeground(new java.awt.Color(255, 255, 255));
        userLogin.setBorder(null);
        userLogin.setMargin(new java.awt.Insets(2, 13, 2, 2));

        passLogin.setBackground(new java.awt.Color(0, 0, 0));
        passLogin.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        passLogin.setForeground(new java.awt.Color(255, 255, 255));
        passLogin.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 1, 0));
        passLogin.setMargin(new java.awt.Insets(2, 13, 2, 2));
        passLogin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                passLoginKeyPressed(evt);
            }
        });

        loginBtn.setBackground(new java.awt.Color(0, 255, 51));
        loginBtn.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        loginBtn.setText("Login");
        loginBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginBtnActionPerformed(evt);
            }
        });

        lblWrong.setForeground(new java.awt.Color(255, 0, 0));
        lblWrong.setText("Your username or password are wrong. Please try again.");

        lblUser.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblUser.setForeground(new java.awt.Color(255, 255, 255));
        lblUser.setText("Username:");

        lblPass.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblPass.setForeground(new java.awt.Color(255, 255, 255));
        lblPass.setText("Password:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(83, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblPass)
                    .addComponent(lblUser)
                    .addComponent(passLogin)
                    .addComponent(userLogin)
                    .addComponent(loginBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(lblWrong)
                        .addGap(24, 24, 24))
                    .addComponent(jSeparator2)
                    .addComponent(jSeparator3))
                .addGap(84, 84, 84))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(134, Short.MAX_VALUE)
                .addComponent(lblUser)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(userLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblPass)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(passLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(lblWrong)
                .addGap(17, 17, 17)
                .addComponent(loginBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74))
        );

        panelBg.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 150, -1, 510));
        panelBg.add(lblBackground, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1060, 700));

        getContentPane().add(panelBg, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 1, 1060, 700));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    public ImageIcon resizeImage(String imgPath) {
        ImageIcon myImage = new ImageIcon(imgPath);
        Image img = myImage.getImage();
        Image newImg = img.getScaledInstance(lblLogo.getWidth(), lblLogo.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImg);
        return image;
    }
    private void loginBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginBtnActionPerformed
        String username = userLogin.getText();
        String password = new String(passLogin.getPassword());
        checkUser(username,password);
        //notChecking();
    }//GEN-LAST:event_loginBtnActionPerformed

    private void passLoginKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_passLoginKeyPressed
        if(evt.getKeyCode()== KeyEvent.VK_ENTER){
            String username = userLogin.getText();
            String password = new String(passLogin.getPassword());
            checkUser(username,password);    
        }
    }//GEN-LAST:event_passLoginKeyPressed

    /**
     * @param args the command line arguments
     */
    public User getUser(){
        return user;
    }
    
    public void notChecking(){
        this.dispose();
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        new MainFrame().setVisible(true);
                    }
                });
    }
    
    private void checkUser(String username, String password){
        for(User u: userList){
            if(u.getUsername().equals(username) && u.getPassword().equals(password)){
                System.out.println("Correct");
                lblWrong.setVisible(false);
                user = u;
                this.dispose();
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        new MainFrame(user).setVisible(true);
                    }
                });

                    
            }else{
                lblWrong.setVisible(true);
                lblWrong.setText("Your username or password are wrong. Please try again.");
            }
        }
    }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }
    public interface onLogin{
        public void logon(User user);
    }
    public void addOnLogin(onLogin subscriber){
        subscribers.add(subscriber);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel lblBackground;
    private javax.swing.JLabel lblLogo;
    private javax.swing.JLabel lblPass;
    private javax.swing.JLabel lblUser;
    private javax.swing.JLabel lblUserIcon;
    private javax.swing.JLabel lblWrong;
    private javax.swing.JButton loginBtn;
    private javax.swing.JPanel panelBg;
    private javax.swing.JPasswordField passLogin;
    private javax.swing.JTextField userLogin;
    // End of variables declaration//GEN-END:variables
}
