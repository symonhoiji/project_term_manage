package ui;


import Component.FontLoader;
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;
import model.User;
import ui.Login.onLogin;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
public class MainFrame extends javax.swing.JFrame implements onLogin {

    /**
     * Creates new form MainFrame
     */
    private int header = 0;
    private User user;
    
     public MainFrame(User user) {
        this.user = user;
        this.initComponents();
        initButton();
        initUser(user);
//        initIconMenu();
        jScrollPane.setViewportView(new MainMenuPanel(user));
        
    }

    public void initIconMenu() {
        try {
            
            
            File fileHome = new File("img/home.PNG");
            BufferedImage imageHome = ImageIO.read(fileHome);
            iconHome.setIcon(new ImageIcon(imageHome));
            
            File fileProduct = new File("img/stock.PNG");
            BufferedImage imageProduct = ImageIO.read(fileProduct);
            iconProduct.setIcon(new ImageIcon(imageProduct));
            
            File fileCustomer = new File("img/customer.PNG");
            BufferedImage imageCustomer = ImageIO.read(fileCustomer);
            iconCustomer.setIcon(new ImageIcon(imageCustomer));
            
            File fileOrder = new File("img/order.PNG");
            BufferedImage imageOrder = ImageIO.read(fileOrder);
            iconOrder.setIcon(new ImageIcon(imageOrder));
            
            File fileUser = new File("img/employee.PNG");
            BufferedImage imageUser = ImageIO.read(fileUser);
            iconUser.setIcon(new ImageIcon(imageUser));
            
            File filePos = new File("img/pos.PNG");
            BufferedImage imagePos = ImageIO.read(filePos);
            iconPos.setIcon(new ImageIcon(imagePos));
            
            File filePromo = new File("img/promo.PNG");
            BufferedImage imagePromo = ImageIO.read(filePromo);
            iconPromotion.setIcon(new ImageIcon(imagePromo));
            
            File fileLogout = new File("img/logout.PNG");
            BufferedImage imageLogout = ImageIO.read(fileLogout);
            iconLogout.setIcon(new ImageIcon(imageLogout));
        } catch (IOException ex) {
            Logger.getLogger(MainMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     

    private void initUser(User myuser) {
        txtName.setText(myuser.getName());
        txtStatus.setText(myuser.getUserType());
        try {
            File file = new File("img/user/" + myuser.getImgPath());
            BufferedImage image = ImageIO.read(file);
            String path = "img/user/" + myuser.getImgPath();
            profileImage.setIcon(resizeImage(path));
        } catch (IOException ex) {
            Logger.getLogger(MainMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        initButtonPermission();
    }
    public ImageIcon resizeImage(String imgPath)
    {
        ImageIcon myImage = new ImageIcon(imgPath);
        Image img = myImage.getImage();
        Image newImg = img.getScaledInstance(profileImage.getWidth(), profileImage.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImg);
        return image;
    }
    private void initButton() {
        homeBtn.setOpaque(false);
        homeBtn.setContentAreaFilled(false);
        homeBtn.setBorderPainted(false);
        
        posBtn.setOpaque(false);
        posBtn.setContentAreaFilled(false);
        posBtn.setBorderPainted(false);
        
        stockBtn.setOpaque(false);
        stockBtn.setContentAreaFilled(false);
        stockBtn.setBorderPainted(false);
        
        orderBtn.setOpaque(false);
        orderBtn.setContentAreaFilled(false);
        orderBtn.setBorderPainted(false);
        
        customerBtn.setOpaque(false);
        customerBtn.setContentAreaFilled(false);
        customerBtn.setBorderPainted(false);
        
        promotionBtn.setOpaque(false);
        promotionBtn.setContentAreaFilled(false);
        promotionBtn.setBorderPainted(false);
        
        userBTN.setOpaque(false);
        userBTN.setContentAreaFilled(false);
        userBTN.setBorderPainted(false);
    }
    public MainFrame() {
        initComponents();
        initButton();
        jScrollPane.setViewportView(new MainMenuPanel(user));
    }
    
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        profileImage = new javax.swing.JLabel();
        lblStatus = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        txtStatus = new javax.swing.JLabel();
        txtName = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        txtHeader = new javax.swing.JLabel();
        logo = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        posBtn = new javax.swing.JButton();
        stockBtn = new javax.swing.JButton();
        promotionBtn = new javax.swing.JButton();
        homeBtn = new javax.swing.JButton();
        logoutBtn = new javax.swing.JButton();
        customerBtn = new javax.swing.JButton();
        userBTN = new javax.swing.JButton();
        orderBtn = new javax.swing.JButton();
        iconUser = new javax.swing.JLabel();
        iconPromotion = new javax.swing.JLabel();
        iconCustomer = new javax.swing.JLabel();
        iconOrder = new javax.swing.JLabel();
        iconProduct = new javax.swing.JLabel();
        iconPos = new javax.swing.JLabel();
        iconHome = new javax.swing.JLabel();
        iconLogout = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        jPanel1.add(jScrollPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 140, 1200, 750));

        jPanel2.setBackground(new java.awt.Color(0, 0, 0));

        profileImage.setForeground(new java.awt.Color(255, 255, 255));
        profileImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        profileImage.setText("Image");

        lblStatus.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblStatus.setForeground(new java.awt.Color(255, 255, 255));
        lblStatus.setText("Status:");

        lblName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblName.setForeground(new java.awt.Color(255, 255, 255));
        lblName.setText("Name:");

        txtStatus.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtStatus.setForeground(new java.awt.Color(255, 255, 255));
        txtStatus.setText("Gernamae");

        txtName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtName.setForeground(new java.awt.Color(255, 255, 255));
        txtName.setText("Testing");

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        txtHeader.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtHeader.setForeground(new java.awt.Color(255, 255, 255));
        txtHeader.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txtHeader.setText("Main Menu");

        logo.setForeground(new java.awt.Color(255, 255, 255));
        logo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(logo, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(txtHeader)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 740, Short.MAX_VALUE)
                .addComponent(profileImage, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblStatus)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtStatus))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtName)))
                .addGap(94, 94, 94))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addComponent(txtHeader))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblName)
                            .addComponent(txtName))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblStatus)
                            .addComponent(txtStatus)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(profileImage, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(logo, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(10, 10, 10))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 120));

        jPanel4.setBackground(new java.awt.Color(251, 215, 196));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        posBtn.setBackground(new java.awt.Color(207, 184, 167));
        posBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        posBtn.setText("Point of Sale");
        posBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        posBtn.setMargin(new java.awt.Insets(2, 65, 2, 14));
        posBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                posMouseHover(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                posMouseExit(evt);
            }
        });
        posBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                posBtnActionPerformed(evt);
            }
        });
        jPanel4.add(posBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 140, 290, 66));

        stockBtn.setBackground(new java.awt.Color(207, 184, 167));
        stockBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        stockBtn.setText("Product Management");
        stockBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        stockBtn.setMargin(new java.awt.Insets(2, 65, 2, 14));
        stockBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                stockMouseHover(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                stockMouseExit(evt);
            }
        });
        stockBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stockBtnActionPerformed(evt);
            }
        });
        jPanel4.add(stockBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 220, 290, 70));

        promotionBtn.setBackground(new java.awt.Color(207, 184, 167));
        promotionBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        promotionBtn.setText("Promotion Management");
        promotionBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        promotionBtn.setMargin(new java.awt.Insets(2, 65, 2, 14));
        promotionBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                empMouseHover(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                empMouseExit(evt);
            }
        });
        promotionBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                promotionBtnActionPerformed(evt);
            }
        });
        jPanel4.add(promotionBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 450, 290, 66));

        homeBtn.setBackground(new java.awt.Color(207, 184, 167));
        homeBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        homeBtn.setText("Home");
        homeBtn.setBorderPainted(false);
        homeBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        homeBtn.setMargin(new java.awt.Insets(2, 65, 2, 14));
        homeBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                homeMouseHover(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                homeMouseExit(evt);
            }
        });
        homeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeBtnActionPerformed(evt);
            }
        });
        jPanel4.add(homeBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 290, 66));

        logoutBtn.setBackground(new java.awt.Color(255, 0, 0));
        logoutBtn.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        logoutBtn.setForeground(new java.awt.Color(255, 255, 255));
        logoutBtn.setText("Log out");
        logoutBtn.setBorder(null);
        logoutBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutBtnActionPerformed(evt);
            }
        });
        jPanel4.add(logoutBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 630, 290, 66));

        customerBtn.setBackground(new java.awt.Color(207, 184, 167));
        customerBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customerBtn.setText("Customer Management");
        customerBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        customerBtn.setMargin(new java.awt.Insets(2, 65, 2, 14));
        customerBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                customerMouseHover(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                customerMouseExit(evt);
            }
        });
        customerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerBtnActionPerformed(evt);
            }
        });
        jPanel4.add(customerBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 380, 290, 60));

        userBTN.setBackground(new java.awt.Color(207, 184, 167));
        userBTN.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        userBTN.setText("User Management");
        userBTN.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        userBTN.setMargin(new java.awt.Insets(2, 65, 2, 14));
        userBTN.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                userMouseHover(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                userMouseExit(evt);
            }
        });
        userBTN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userBTNActionPerformed(evt);
            }
        });
        jPanel4.add(userBTN, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 530, 290, 60));

        orderBtn.setBackground(new java.awt.Color(207, 184, 167));
        orderBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        orderBtn.setText("Order Management");
        orderBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        orderBtn.setMargin(new java.awt.Insets(2, 65, 2, 14));
        orderBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                orderMouseHover(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                orderMouseExit(evt);
            }
        });
        orderBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                orderBtnActionPerformed(evt);
            }
        });
        jPanel4.add(orderBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 300, 290, 70));
        jPanel4.add(iconUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 540, 40, 40));
        jPanel4.add(iconPromotion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 460, 40, 40));
        jPanel4.add(iconCustomer, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 390, 40, 40));
        jPanel4.add(iconOrder, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 310, 40, 40));
        jPanel4.add(iconProduct, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, 40, 40));
        jPanel4.add(iconPos, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 40, 40));
        jPanel4.add(iconHome, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 40, 40));
        jPanel4.add(iconLogout, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 640, 40, 40));

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, 290, 770));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1490, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void orderBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_orderBtnActionPerformed
        jScrollPane.setViewportView(new OrderPanel());
        txtHeader.setText("Order Management");
    }//GEN-LAST:event_orderBtnActionPerformed

    private void userBTNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userBTNActionPerformed
        jScrollPane.setViewportView(new UserManagementPanel());
        txtHeader.setText("User Management");
    }//GEN-LAST:event_userBTNActionPerformed

    private void customerBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerBtnActionPerformed
        jScrollPane.setViewportView(new MemberPanel());
        txtHeader.setText("Customer Management");
    }//GEN-LAST:event_customerBtnActionPerformed

    private void homeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeBtnActionPerformed
       jScrollPane.setViewportView(new MainMenuPanel(user));
       txtHeader.setText("Main Menu");
       
    }//GEN-LAST:event_homeBtnActionPerformed

    private void homeMouseHover(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_homeMouseHover
        homeBtn.setContentAreaFilled(true);
    }//GEN-LAST:event_homeMouseHover

    private void homeMouseExit(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_homeMouseExit
        homeBtn.setContentAreaFilled(false);
    }//GEN-LAST:event_homeMouseExit

    private void logoutBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutBtnActionPerformed
        this.dispose();
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }//GEN-LAST:event_logoutBtnActionPerformed

    private void stockBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stockBtnActionPerformed
        txtHeader.setText("Product Management");
        jScrollPane.setViewportView(new ProductPanel());
    }//GEN-LAST:event_stockBtnActionPerformed

    private void posMouseHover(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_posMouseHover
        posBtn.setContentAreaFilled(true);
    }//GEN-LAST:event_posMouseHover

    private void posMouseExit(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_posMouseExit
        posBtn.setContentAreaFilled(false);
    }//GEN-LAST:event_posMouseExit

    private void stockMouseHover(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stockMouseHover
        stockBtn.setContentAreaFilled(true);
    }//GEN-LAST:event_stockMouseHover

    private void stockMouseExit(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stockMouseExit
        stockBtn.setContentAreaFilled(false);
    }//GEN-LAST:event_stockMouseExit

    private void orderMouseHover(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_orderMouseHover
        orderBtn.setContentAreaFilled(true);
    }//GEN-LAST:event_orderMouseHover

    private void orderMouseExit(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_orderMouseExit
        orderBtn.setContentAreaFilled(false);
    }//GEN-LAST:event_orderMouseExit

    private void customerMouseHover(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_customerMouseHover
        customerBtn.setContentAreaFilled(true);
    }//GEN-LAST:event_customerMouseHover

    private void customerMouseExit(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_customerMouseExit
        customerBtn.setContentAreaFilled(false);
    }//GEN-LAST:event_customerMouseExit

    private void empMouseHover(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_empMouseHover
        promotionBtn.setContentAreaFilled(true);
    }//GEN-LAST:event_empMouseHover

    private void empMouseExit(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_empMouseExit
        promotionBtn.setContentAreaFilled(false);
    }//GEN-LAST:event_empMouseExit

    private void userMouseHover(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_userMouseHover
        userBTN.setContentAreaFilled(true);
    }//GEN-LAST:event_userMouseHover

    private void userMouseExit(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_userMouseExit
        userBTN.setContentAreaFilled(false);
    }//GEN-LAST:event_userMouseExit

    private void promotionBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_promotionBtnActionPerformed
        jScrollPane.setViewportView(new PromotionPanel());
        txtHeader.setText("Promotion Management");
    }//GEN-LAST:event_promotionBtnActionPerformed

    private void posBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_posBtnActionPerformed
        txtHeader.setText("Point of Sale");
        jScrollPane.setViewportView(new PointofSalePanel(user));
    }//GEN-LAST:event_posBtnActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton customerBtn;
    private javax.swing.JButton homeBtn;
    private javax.swing.JLabel iconCustomer;
    private javax.swing.JLabel iconHome;
    private javax.swing.JLabel iconLogout;
    private javax.swing.JLabel iconOrder;
    private javax.swing.JLabel iconPos;
    private javax.swing.JLabel iconProduct;
    private javax.swing.JLabel iconPromotion;
    private javax.swing.JLabel iconUser;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel logo;
    private javax.swing.JButton logoutBtn;
    private javax.swing.JButton orderBtn;
    private javax.swing.JButton posBtn;
    private javax.swing.JLabel profileImage;
    private javax.swing.JButton promotionBtn;
    private javax.swing.JButton stockBtn;
    private javax.swing.JLabel txtHeader;
    private javax.swing.JLabel txtName;
    private javax.swing.JLabel txtStatus;
    private javax.swing.JButton userBTN;
    // End of variables declaration//GEN-END:variables

    @Override
    public void logon(User user) {
        
        System.out.println(user);
    }

    private void initButtonPermission() {
        if(user.getUserType().equals("Employee")){
            orderBtn.setVisible(false);
            customerBtn.setVisible(false);
            userBTN.setVisible(false);
            promotionBtn.setVisible(false);
        }else{
            try{
                File fileIconHome = new File("img/logo250x90.PNG");
                BufferedImage imageIconHome = ImageIO.read(fileIconHome);
                logo.setIcon(new ImageIcon(imageIconHome));
                
                File fileOrder = new File("img/order.PNG");
                BufferedImage imageOrder = ImageIO.read(fileOrder);
                iconOrder.setIcon(new ImageIcon(imageOrder));

                File fileUser = new File("img/employee.PNG");
                BufferedImage imageUser = ImageIO.read(fileUser);
                iconUser.setIcon(new ImageIcon(imageUser));

                File filePos = new File("img/pos.PNG");
                BufferedImage imagePos = ImageIO.read(filePos);
                iconPos.setIcon(new ImageIcon(imagePos));

                File fileCustomer = new File("img/customer.PNG");
                BufferedImage imageCustomer = ImageIO.read(fileCustomer);
                iconCustomer.setIcon(new ImageIcon(imageCustomer));
                
                File filePromo = new File("img/promo.PNG");
                BufferedImage imagePromo = ImageIO.read(filePromo);
                iconPromotion.setIcon(new ImageIcon(imagePromo));
                
                File fileLogout = new File("img/logout.PNG");
                BufferedImage imageLogout = ImageIO.read(fileLogout);
                iconLogout.setIcon(new ImageIcon(imageLogout));
            } catch (IOException ex) {
                Logger.getLogger(MainMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        try{
            File fileHome = new File("img/home.PNG");
            BufferedImage imageHome = ImageIO.read(fileHome);
            iconHome.setIcon(new ImageIcon(imageHome));
            
            File filePos = new File("img/pos.PNG");
            BufferedImage imagePos = ImageIO.read(filePos);
            iconPos.setIcon(new ImageIcon(imagePos));
            
            File fileProduct = new File("img/stock.PNG");
            BufferedImage imageProduct = ImageIO.read(fileProduct);
            iconProduct.setIcon(new ImageIcon(imageProduct));

        } catch (IOException ex) {
            Logger.getLogger(MainMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }



}
