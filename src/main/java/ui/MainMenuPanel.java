/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;
import dao.ProductDao;
import dao.TimeStampDao;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;
import model.Product;
import model.TimeStamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import model.User;

/**
 *
 * @author Mr.Omega
 */
public class MainMenuPanel extends javax.swing.JPanel {
    private final ProductDao pdDao = new ProductDao();
    private final TimeStampDao tDao = new TimeStampDao();
    private ArrayList<Product> productList;
    private ArrayList<TimeStamp> timeList;
    private StockTableModel model;
    private TimeTableModel timemodel;
    User user;
    private int countRow=1;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
    private SimpleDateFormat simpleDate= new SimpleDateFormat("EEE, d MMM yyyy");
    /**
     * Creates new form MainMenuPanel
     */
    public MainMenuPanel() {
        initComponents();
        productBestWorst();
        
        
    }
    public MainMenuPanel(User user) {
        this.user=user;
        initComponents();
        productBestWorst();
        chekSignIn(tDao);
        loadTableTime(tDao);
        chekSignOut(tDao);
        LiveDateSwing();
        loadDate();
    }
    
    public void LiveDateSwing() {
      Runnable runnable = new Runnable() {

        @Override
        public void run() {
          while (true) {
            Date date = getDate();
            String dateString = simpleDateFormat.format(date);
            lblTime.setText(dateString);
            try {
              Thread.sleep(1000);
            }
            catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
    };

      Thread t = new Thread(runnable);
      t.start();
    }
    public static java.util.Date getDate() {
      java.util.Date date = new java.util.Date();
      return date;
    }
    
    private void loadDate() {       
        String dateString = simpleDate.format(new java.util.Date());
        lblDate.setText(dateString);
    }
    
    
    
    private void productBestWorst() {
        Product productBest = pdDao.getBestSell();
        Product productWorse = pdDao.getWorseSell();
        txtBestSell.setText(productBest.getName());
        txtPriceBestSell.setText(""+productBest.getSale()+" Pieces");
        txtWorstSell.setText(productWorse.getName());
        txtPriceWorstSell.setText(""+productWorse.getSale()+" Pieces");
        loadImageBest(productBest);
        loadImageWorst(productWorse);
        loadTable(pdDao);


        
    }
    private void chekSignIn(TimeStampDao dao) {       
         timeList = dao.getAll(user.getId());
         LocalDate date = LocalDate.now();
         DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
         
         for(TimeStamp temp:timeList){
             if(date.format(formatter).equals(temp.getDate())){
                btnSignIn.setEnabled(false);
                btnSignIn.setText("Sign In");
             }
                 
         }
    }
    private void chekSignOut(TimeStampDao dao) {       
         timeList = dao.getAll(user.getId());
         LocalDate date = LocalDate.now();
         DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
         for(TimeStamp temp:timeList){
             if(date.format(formatter).equals(temp.getDate())&&temp.getSignOut()!=null){
                btnSignOut.setEnabled(false);
                btnSignOut.setText("Sign out");
             }
                 
         }
    }
    public ImageIcon resizeImage(String imgPath) {
        ImageIcon myImage = new ImageIcon(imgPath);
        Image img = myImage.getImage();
        Image newImg = img.getScaledInstance(215, 150, Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImg);
        return image;
    }
    private void loadImageBest(Product p) {       
        try {
            File file=new File("img/product/"+p.getImg());
            BufferedImage image = ImageIO.read(file);
            String path = "img/product/"+p.getImg();
            imageBestSell.setIcon(resizeImage(path));
        } catch (IOException ex) {
            Logger.getLogger(MainMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void loadImageWorst(Product p) {       
        try {
            File file=new File("img/product/"+p.getImg());
            BufferedImage image = ImageIO.read(file);
            String path = "img/product/"+p.getImg();
            imageWorstSell.setIcon(resizeImage(path));
        } catch (IOException ex) {
            Logger.getLogger(MainMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void loadTable(ProductDao dao) {
        productList = dao.getAllByRemain();
        model = new StockTableModel(productList);
        tblAmount.setModel(model);
    }
    public void loadTableTime(TimeStampDao dao) {
        timeList = dao.getAll(user.getId());
        timemodel = new TimeTableModel(timeList);
        tblTimeStamp.setModel(timemodel);
    }
     
     
    private class StockTableModel extends AbstractTableModel{
        
        private final ArrayList<Product> productList;
        String columnName[] = {"Stock ID", "Product Name","Remainder(Piece)"};
        public StockTableModel(ArrayList<Product> productList) {
            this.productList = productList;
        }
        
        @Override
        public int getRowCount() {
            return this.productList.size();
        }

        @Override
        public int getColumnCount() {
            return 3;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Product product = this.productList.get(rowIndex);
            if(columnIndex == 0){
                return countRow+rowIndex;
            }
            if(columnIndex == 1){
                return product.getName();
            }
            if(columnIndex == 2){
                return product.getRemaining();
            }
            return "";
        }

        @Override
        public String getColumnName(int column) {
            return columnName[column];
        }
        
    
}
    
        private class TimeTableModel extends AbstractTableModel{
        
            private final ArrayList<TimeStamp> timeList;
            String columnName[] = {"No.", "SignIn Time","SignOut Time","Date"};
            public TimeTableModel(ArrayList<TimeStamp> timeList) {
                this.timeList = timeList;
            }

            @Override
            public int getRowCount() {
                return this.timeList.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                TimeStamp time = this.timeList.get(rowIndex);
                if(columnIndex == 0){
                    return countRow+rowIndex;
                }
                if(columnIndex == 1){
                    return time.getSignIn();
                }
                if(columnIndex == 2){
                    return time.getSignOut();
                }
                if(columnIndex == 3){
                    return time.getDate();
                }
                return "";
            }

            @Override
            public String getColumnName(int column) {
                return columnName[column];
            }


    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnSales = new javax.swing.JPanel();
        pnTable = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblAmount = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        pnBest = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        imageBestSell = new javax.swing.JLabel();
        txtBestSell = new javax.swing.JLabel();
        txtPriceBestSell = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        pnBest1 = new javax.swing.JPanel();
        txtWorstSell = new javax.swing.JLabel();
        txtPriceWorstSell = new javax.swing.JLabel();
        imageWorstSell = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        pnCheckIn = new javax.swing.JPanel();
        btnSignIn = new javax.swing.JButton();
        btnSignOut = new javax.swing.JButton();
        pnTimeStamp = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblTimeStamp = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        lblTime = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(1200, 750));
        setMinimumSize(new java.awt.Dimension(1200, 750));
        setPreferredSize(new java.awt.Dimension(1200, 750));

        pnSales.setBackground(new java.awt.Color(255, 255, 255));
        pnSales.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 102, 0), 2));
        pnSales.setPreferredSize(new java.awt.Dimension(600, 214));

        pnTable.setBackground(new java.awt.Color(255, 255, 255));
        pnTable.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 102, 0), 0));
        pnTable.setPreferredSize(new java.awt.Dimension(600, 214));

        jScrollPane1.setBorder(null);

        tblAmount.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblAmount);

        jPanel4.setBackground(new java.awt.Color(204, 102, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Amount In Stock");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout pnTableLayout = new javax.swing.GroupLayout(pnTable);
        pnTable.setLayout(pnTableLayout);
        pnTableLayout.setHorizontalGroup(
            pnTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnTableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnTableLayout.setVerticalGroup(
            pnTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnTableLayout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnBest.setBackground(new java.awt.Color(0, 0, 0));
        pnBest.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(231, 107, 36), 5));
        pnBest.setPreferredSize(new java.awt.Dimension(280, 332));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(231, 107, 36));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Best");

        imageBestSell.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        imageBestSell.setText("image");
        imageBestSell.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        imageBestSell.setPreferredSize(new java.awt.Dimension(215, 150));

        txtBestSell.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtBestSell.setForeground(new java.awt.Color(255, 255, 255));
        txtBestSell.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtBestSell.setText("Hot Espresso");

        txtPriceBestSell.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtPriceBestSell.setForeground(new java.awt.Color(231, 107, 36));
        txtPriceBestSell.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtPriceBestSell.setText("72 Pieces");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel6.setText("Seller");

        javax.swing.GroupLayout pnBestLayout = new javax.swing.GroupLayout(pnBest);
        pnBest.setLayout(pnBestLayout);
        pnBestLayout.setHorizontalGroup(
            pnBestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(txtBestSell, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnBestLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtPriceBestSell, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(pnBestLayout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 14, Short.MAX_VALUE))
            .addGroup(pnBestLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(imageBestSell, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnBestLayout.setVerticalGroup(
            pnBestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnBestLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnBestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(imageBestSell, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtBestSell, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPriceBestSell, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnBest1.setBackground(new java.awt.Color(0, 0, 0));
        pnBest1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(231, 107, 36), 5));
        pnBest1.setPreferredSize(new java.awt.Dimension(280, 332));

        txtWorstSell.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtWorstSell.setForeground(new java.awt.Color(255, 255, 255));
        txtWorstSell.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtWorstSell.setText("Hot Latte");

        txtPriceWorstSell.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtPriceWorstSell.setForeground(new java.awt.Color(231, 107, 36));
        txtPriceWorstSell.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtPriceWorstSell.setText("72 Pieces");

        imageWorstSell.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        imageWorstSell.setText("image");
        imageWorstSell.setPreferredSize(new java.awt.Dimension(215, 150));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("Seller");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(231, 107, 36));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Worst");

        javax.swing.GroupLayout pnBest1Layout = new javax.swing.GroupLayout(pnBest1);
        pnBest1.setLayout(pnBest1Layout);
        pnBest1Layout.setHorizontalGroup(
            pnBest1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnBest1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(imageWorstSell, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(pnBest1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnBest1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPriceWorstSell, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnBest1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addComponent(txtWorstSell, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnBest1Layout.setVerticalGroup(
            pnBest1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnBest1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(pnBest1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(imageWorstSell, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtWorstSell, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPriceWorstSell, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout pnSalesLayout = new javax.swing.GroupLayout(pnSales);
        pnSales.setLayout(pnSalesLayout);
        pnSalesLayout.setHorizontalGroup(
            pnSalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnSalesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnBest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(pnBest1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(pnTable, javax.swing.GroupLayout.DEFAULT_SIZE, 596, Short.MAX_VALUE)
        );
        pnSalesLayout.setVerticalGroup(
            pnSalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnSalesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnSalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnBest1, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                    .addComponent(pnBest, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnTable, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 102, 0), 2));

        pnCheckIn.setBackground(new java.awt.Color(255, 255, 255));
        pnCheckIn.setPreferredSize(new java.awt.Dimension(600, 214));

        btnSignIn.setBackground(new java.awt.Color(0, 51, 153));
        btnSignIn.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        btnSignIn.setForeground(new java.awt.Color(255, 255, 255));
        btnSignIn.setText("SignIn");
        btnSignIn.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 0, true));
        btnSignIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSignInActionPerformed(evt);
            }
        });

        btnSignOut.setBackground(new java.awt.Color(204, 51, 0));
        btnSignOut.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        btnSignOut.setForeground(new java.awt.Color(255, 255, 255));
        btnSignOut.setText("SignOut");
        btnSignOut.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 0, true));
        btnSignOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSignOutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnCheckInLayout = new javax.swing.GroupLayout(pnCheckIn);
        pnCheckIn.setLayout(pnCheckInLayout);
        pnCheckInLayout.setHorizontalGroup(
            pnCheckInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCheckInLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(btnSignIn, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSignOut, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40))
        );
        pnCheckInLayout.setVerticalGroup(
            pnCheckInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCheckInLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnCheckInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSignOut, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSignIn, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tblTimeStamp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblTimeStamp);

        jPanel2.setBackground(new java.awt.Color(204, 102, 0));

        jLabel4.setBackground(new java.awt.Color(0, 0, 0));
        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Time Stamp");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(9, 9, 9))
        );

        javax.swing.GroupLayout pnTimeStampLayout = new javax.swing.GroupLayout(pnTimeStamp);
        pnTimeStamp.setLayout(pnTimeStampLayout);
        pnTimeStampLayout.setHorizontalGroup(
            pnTimeStampLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnTimeStampLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnTimeStampLayout.setVerticalGroup(
            pnTimeStampLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnTimeStampLayout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(204, 102, 0));

        lblTime.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lblTime.setForeground(new java.awt.Color(255, 255, 255));
        lblTime.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTime.setText("Time Online");

        lblDate.setFont(new java.awt.Font("Tahoma", 1, 28)); // NOI18N
        lblDate.setForeground(new java.awt.Color(255, 255, 255));
        lblDate.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDate.setText("Time Online");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTime, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblDate, javax.swing.GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addComponent(lblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblDate, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnTimeStamp, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnCheckIn, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(pnTimeStamp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(pnCheckIn, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(pnSales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnSales, javax.swing.GroupLayout.DEFAULT_SIZE, 716, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(17, 17, 17))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSignOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSignOutActionPerformed
         timeList = tDao.getAll(user.getId());
         LocalDate date = LocalDate.now();
         DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
         for(TimeStamp temp:timeList){
             if(date.format(formatter).equals(temp.getDate())&&temp.getSignOut()==null){
                tDao.update(temp);
             }        
         }
        loadTableTime(tDao);
        chekSignOut(tDao);
        
        
    }//GEN-LAST:event_btnSignOutActionPerformed

    private void btnSignInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSignInActionPerformed
        TimeStamp t=new TimeStamp(user);
        tDao.add(t);
        chekSignIn(tDao);
        loadTableTime(tDao);
    }//GEN-LAST:event_btnSignInActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSignIn;
    private javax.swing.JButton btnSignOut;
    private javax.swing.JLabel imageBestSell;
    private javax.swing.JLabel imageWorstSell;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblTime;
    private javax.swing.JPanel pnBest;
    private javax.swing.JPanel pnBest1;
    private javax.swing.JPanel pnCheckIn;
    private javax.swing.JPanel pnSales;
    private javax.swing.JPanel pnTable;
    private javax.swing.JPanel pnTimeStamp;
    private javax.swing.JTable tblAmount;
    private javax.swing.JTable tblTimeStamp;
    private javax.swing.JLabel txtBestSell;
    private javax.swing.JLabel txtPriceBestSell;
    private javax.swing.JLabel txtPriceWorstSell;
    private javax.swing.JLabel txtWorstSell;
    // End of variables declaration//GEN-END:variables
}
