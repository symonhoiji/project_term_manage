/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import dao.OrderDao;
import java.awt.Color;
import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import model.Order;

/**
 *
 * @author Felidas
 */
public class OrderPanel extends javax.swing.JPanel {

    private ArrayList<Order> OrderList;
    private OrderTableModel model;
    OrderDao dao = new OrderDao();
    private int index = -1; 

    /**
     * Creates new form StockPanel
     */
    public OrderPanel() {
        initComponents();
        loadTable(dao);
        loadImage();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane = new javax.swing.JScrollPane();
        tblOrder = new javax.swing.JTable();
        txtSearch = new javax.swing.JTextField();
        btnViewDetailOrder = new javax.swing.JButton();
        lblIconSearch = new javax.swing.JLabel();
        lblIconView = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(1250, 750));
        setMinimumSize(new java.awt.Dimension(1200, 750));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane.setMaximumSize(new java.awt.Dimension(1200, 750));
        jScrollPane.setMinimumSize(new java.awt.Dimension(1200, 750));
        jScrollPane.setName(""); // NOI18N
        jScrollPane.setPreferredSize(new java.awt.Dimension(1200, 750));

        tblOrder.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "ID", "Food Ingredients Name", "Ouantity (Piece)", "Edit", "Remove"
            }
        ));
        jScrollPane.setViewportView(tblOrder);

        add(jScrollPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 114, 1141, 525));

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch.setForeground(new java.awt.Color(128, 128, 128));
        txtSearch.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSearch.setText("Search User Name");
        txtSearch.setMaximumSize(new java.awt.Dimension(140, 25));
        txtSearch.setMinimumSize(new java.awt.Dimension(140, 25));
        txtSearch.setPreferredSize(new java.awt.Dimension(140, 25));
        txtSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtSearchMouseClicked(evt);
            }
        });
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(92, 42, 236, 49));

        btnViewDetailOrder.setBackground(new java.awt.Color(0, 102, 204));
        btnViewDetailOrder.setText("View Detail Order");
        btnViewDetailOrder.setMaximumSize(new java.awt.Dimension(90, 25));
        btnViewDetailOrder.setMinimumSize(new java.awt.Dimension(90, 25));
        btnViewDetailOrder.setPreferredSize(new java.awt.Dimension(90, 25));
        btnViewDetailOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewDetailOrderActionPerformed(evt);
            }
        });
        add(btnViewDetailOrder, new org.netbeans.lib.awtextra.AbsoluteConstraints(1018, 43, 150, 58));
        add(lblIconSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 37, 55, 55));
        add(lblIconView, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 50, 60, 50));
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewDetailOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewDetailOrderActionPerformed
        index = tblOrder.getSelectedRow();
        if(index<0){
            JFrame frame = new JFrame("JOptionPane showMessageError");
            JOptionPane.showMessageDialog(frame,
            "Please Select Order",
            "Notification",
            JOptionPane.ERROR_MESSAGE);
            return ;}
        ShowDetailOrder showDOrder = new ShowDetailOrder(OrderList.get(tblOrder.getSelectedRow()).getId());
        showDOrder.setVisible(true);
    }//GEN-LAST:event_btnViewDetailOrderActionPerformed

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed

    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtSearchMouseClicked
        txtSearch.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchMouseClicked

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        loadSearch();
    }//GEN-LAST:event_txtSearchKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnViewDetailOrder;
    private javax.swing.JScrollPane jScrollPane;
    private javax.swing.JLabel lblIconSearch;
    private javax.swing.JLabel lblIconView;
    private javax.swing.JTable tblOrder;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

    private void loadTable(OrderDao dao) {
        OrderList = dao.getAll();
        model = new OrderTableModel(OrderList);
        tblOrder.setModel(model);
    }

    private void loadSearch() {
        OrderList = dao.Search(txtSearch.getText());
        model = new OrderTableModel(OrderList);
        tblOrder.setModel(model);
    }

    private void loadImage() {

        try {
            File file = new File("img/Search.JPG");
            BufferedImage image = ImageIO.read(file);
            lblIconSearch.setIcon(new ImageIcon(image));
            
            File fileView = new File("img/view.PNG");
            BufferedImage imageView = ImageIO.read(fileView);
            lblIconView.setIcon(new ImageIcon(imageView));
        } catch (IOException ex) {
            Logger.getLogger(OrderPanel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
//test    
//    private class HeaderRenderer implements TableCellRenderer {
//
//    DefaultTableCellRenderer renderer;
//
//    public HeaderRenderer(JTable table) {
//        renderer = (DefaultTableCellRenderer)
//            table.getTableHeader().getDefaultRenderer();
//        renderer.setHorizontalAlignment(JLabel.CENTER);
//    }
//
//    @Override
//    public Component getTableCellRendererComponent(
//        JTable table, Object value, boolean isSelected,
//        boolean hasFocus, int row, int col) {
//        return renderer.getTableCellRendererComponent(
//            table, value, isSelected, hasFocus, row, col);
//    }
//}
    private void initDisabledButton() {

        btnViewDetailOrder.setEnabled(false);
        }

    private class OrderTableModel extends AbstractTableModel {

        private ArrayList<Order> data;
        String columnName[] = {"No.","Order ID", "User Name","Promotion", "Time", "Member", "Total"};

        public OrderTableModel(ArrayList<Order> data) {
            super();
            this.data = data;
        }

        @Override
        public int getRowCount() {
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return 7;
        }
        

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Order order = this.data.get(rowIndex);
            order.getDetailorder().toString();
            if (columnIndex == 0) {
                return rowIndex+1;
            }
            if (columnIndex == 1) {
                return order.getId();
            }
            if (columnIndex == 2) {
                return order.getSeller().getName();
            }
            if (columnIndex == 3) {
                return order.getPromotion().getDesc();
            }
            if (columnIndex == 4) {
                return order.getCreated();
            }
            if (columnIndex == 5) {
                return order.getMember().getName();
            }
            if (columnIndex == 6) {
                return order.getTotal();
            }
            return "";
        }

        @Override
        public String getColumnName(int column) {
            return columnName[column];
        }

    }

}
