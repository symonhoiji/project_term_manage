package ui;

import Component.AddPromotion;
import Component.EditPromotion;
import Component.RemovePromotion;
import java.util.ArrayList;
import model.Promotion;
import dao.PromotionDao;
import Component.ButtonColumn;
import dao.MemberDao;
import dao.ProductDao;
import java.awt.Color;
import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import model.Product;
import static ui.PointofSalePanel.setColumnWidths;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class PromotionPanel extends javax.swing.JPanel {

    private ArrayList<Promotion> promotionList = new ArrayList();
    private PromotionTableModel model;
    PromotionDao dao = new PromotionDao();
    Promotion editedPromotion, removePromotion;

    public PromotionPanel() {
        initComponents();
        loadTable(dao);
       loadImage();
    }
   public PromotionPanel(ArrayList<Promotion> list){
       initComponents();
        list = dao.getAll();
        promotionList.addAll(list);
    }
    public void refreshTable() {
        PromotionDao dao = new PromotionDao();
        ArrayList<Promotion> newList = dao.getAll();
        promotionList = newList;
        PromotionTableModel model = new PromotionTableModel(promotionList);
        tblPromotion.setModel(model);
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnEdit = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblPromotion = new javax.swing.JTable();
        txtSearch = new javax.swing.JTextField();
        lblIconSearch = new javax.swing.JLabel();
        lblIconAdd = new javax.swing.JLabel();
        lblIconEdit = new javax.swing.JLabel();
        lblIconRemove = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setToolTipText("");
        setMaximumSize(new java.awt.Dimension(1250, 750));
        setMinimumSize(new java.awt.Dimension(1250, 750));
        setPreferredSize(new java.awt.Dimension(1250, 750));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnEdit.setBackground(new java.awt.Color(255, 255, 0));
        btnEdit.setText("Edit Promotion");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        add(btnEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 40, 120, 60));

        btnAdd.setBackground(new java.awt.Color(128, 255, 0));
        btnAdd.setText("Add Promotion");
        btnAdd.setMaximumSize(new java.awt.Dimension(90, 25));
        btnAdd.setMinimumSize(new java.awt.Dimension(90, 25));
        btnAdd.setPreferredSize(new java.awt.Dimension(90, 25));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        add(btnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 40, 130, 60));

        btnDelete.setBackground(new java.awt.Color(255, 0, 0));
        btnDelete.setText("Remove Promotion");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 40, -1, 60));

        tblPromotion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblPromotion);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(31, 111, 1140, 540));

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch.setForeground(new java.awt.Color(100, 100, 100));
        txtSearch.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSearch.setText(" Search Promotion Percent");
        txtSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtSearchMouseClicked(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 50, 220, 50));

        lblIconSearch.setMaximumSize(new java.awt.Dimension(55, 55));
        lblIconSearch.setMinimumSize(new java.awt.Dimension(55, 55));
        lblIconSearch.setPreferredSize(new java.awt.Dimension(55, 55));
        add(lblIconSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 60, 50));

        lblIconAdd.setMaximumSize(new java.awt.Dimension(55, 55));
        lblIconAdd.setMinimumSize(new java.awt.Dimension(55, 55));
        lblIconAdd.setPreferredSize(new java.awt.Dimension(55, 55));
        add(lblIconAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 50, 60, 50));

        lblIconEdit.setMaximumSize(new java.awt.Dimension(55, 55));
        lblIconEdit.setMinimumSize(new java.awt.Dimension(55, 55));
        lblIconEdit.setPreferredSize(new java.awt.Dimension(55, 55));
        add(lblIconEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 50, 60, 50));

        lblIconRemove.setMaximumSize(new java.awt.Dimension(55, 55));
        lblIconRemove.setMinimumSize(new java.awt.Dimension(55, 55));
        lblIconRemove.setPreferredSize(new java.awt.Dimension(55, 55));
        add(lblIconRemove, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 50, 60, 50));
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        AddPromotion add = new AddPromotion(this);
        add.setVisible(true);
        add.setLocationRelativeTo(null);
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        if(tblPromotion.getSelectedRow() >= 0){
            editedPromotion = promotionList.get(tblPromotion.getSelectedRow());
            EditPromotion edit = new EditPromotion(editedPromotion,this);
            edit.setVisible(true);
            edit.setLocationRelativeTo(null);
            
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        if(tblPromotion.getSelectedRow() >= 0){
            removePromotion = promotionList.get(tblPromotion.getSelectedRow());
            RemovePromotion remove = new RemovePromotion(removePromotion,this);
            remove.setVisible(true);
            remove.setLocationRelativeTo(null);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void txtSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtSearchMouseClicked
        txtSearch.setText("");
    }//GEN-LAST:event_txtSearchMouseClicked

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
       loadSearch(dao);
    }//GEN-LAST:event_txtSearchKeyReleased

 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblIconAdd;
    private javax.swing.JLabel lblIconEdit;
    private javax.swing.JLabel lblIconRemove;
    private javax.swing.JLabel lblIconSearch;
    private javax.swing.JTable tblPromotion;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

    private void loadTable(PromotionDao dao) {
        promotionList = dao.getAll();
        model = new PromotionTableModel(promotionList);
        tblPromotion.setModel(model);
    }

    private void loadSearch(PromotionDao dao) {
        promotionList = dao.Search(Integer.parseInt(txtSearch.getText()));
        model = new PromotionTableModel(promotionList);
        tblPromotion.setModel(model);
    }
    private void loadImage() {
        try {
            File fileAdd = new File("img/icon add product.JPG");
            BufferedImage imageAdd = ImageIO.read(fileAdd);
            lblIconAdd.setIcon(new ImageIcon(imageAdd));
            
            File fileSearch = new File("img/Search.JPG");
            BufferedImage imageSearch = ImageIO.read(fileSearch);
            lblIconSearch.setIcon(new ImageIcon(imageSearch));
            
            File fileEdit = new File("img/edit.PNG");
            BufferedImage imageEdit = ImageIO.read(fileEdit);
            lblIconEdit.setIcon(new ImageIcon(imageEdit));
            
            File fileRemove = new File("img/rem.PNG");
            BufferedImage imageRemove = ImageIO.read(fileRemove);
            lblIconRemove.setIcon(new ImageIcon(imageRemove));
        } catch (IOException ex) {
            Logger.getLogger(ProductPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    private class PromotionTableModel extends AbstractTableModel {

        private ArrayList<Promotion> data;
        String columnName[] = {"Promotion ID", "Discount Percent", "Discount Type", "Description", "Status"};

        public PromotionTableModel(ArrayList<Promotion> data) {
            this.data = data;
        }
      

        @Override
        public int getRowCount() {
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return 5;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Promotion promotion = this.data.get(rowIndex);
            if (columnIndex == 0) {
                return promotion.getId();
            }
            if (columnIndex == 1) {
                return promotion.getDiscount();
            }
            if (columnIndex == 2) {
                return promotion.getType();
            }
            if (columnIndex == 3) {
                return promotion.getDesc();
            }
            if (columnIndex == 4) {
                return promotion.getStatus();
            }
            return "";
        }

        public String getColumnName(int column) {
            return columnName[column];
        }
    }
}
