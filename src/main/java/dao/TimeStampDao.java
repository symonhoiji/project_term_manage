/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.TimeStamp;
import model.User;

/**
 *
 * @author admin
 */
public class TimeStampDao implements DaoInterface<TimeStamp> {



    @Override
    public TimeStamp get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM TimeStamp t,user u WHERE t.ts_date =CURRENT_DATE AND t.user_id =" +id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()){
                int Tid = result.getInt("ts_id");
                String signIn = result.getString("ts_sign_in");
                String signOut = result.getString("ts_sign_out");
                String created = result.getString("ts_date");
                
                
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userUsername = result.getString("user_username");
                String userPassword = result.getString("user_password");
                String userEmail = result.getString("user_email");
                String userType = result.getString("user_type");
                double userSalary = result.getDouble("user_salary");
                String userTel = result.getString("user_tel");
                String userImg = result.getString("user_img");
                TimeStamp time = new TimeStamp(Tid,signIn,signOut,created,new User(userId, userUsername, userPassword, userName, userEmail, userType, userSalary, userTel, userImg));
                return time;
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int update(TimeStamp object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE timestamp SET ts_sign_out = CURRENT_TIME WHERE ts_id = ? ;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1,object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }

       db.close();
       return row;
    }

    @Override
    public int add(TimeStamp object) {
         Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
         try {
            String sql = "INSERT INTO timestamp (\n" +
"                          user_id\n" +
"                          )\n" + "VALUES (?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getUser().getId());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
           

        } catch (SQLException ex) {
            System.out.println("Error: to create TimeStamp");
        }
        
        
        return id;
    }

    @Override
    public ArrayList<TimeStamp> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ArrayList<TimeStamp> getAll(int getId){
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM TimeStamp t,user u WHERE t.user_id=? AND t.user_id = u.user_id";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, getId);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                int id = result.getInt("ts_id");
                String signIn = result.getString("ts_sign_in");
                String signOut = result.getString("ts_sign_out");
                String created = result.getString("ts_date");

                
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userUsername = result.getString("user_username");
                String userPassword = result.getString("user_password");
                String userEmail = result.getString("user_email");
                String userType = result.getString("user_type");
                double userSalary = result.getDouble("user_salary");
                String userTel = result.getString("user_tel");
                String userImg = result.getString("user_img");
                
                TimeStamp time = new TimeStamp(id,signIn,signOut,created,new User(userId, userUsername, userPassword, userName, userEmail, userType, userSalary, userTel, userImg));
                list.add(time);
            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return list;
    
    }
//    public static void main(String[] args) {
//        TimeStamp u=new TimeStamp(new User(1, "admin", "123", "Admin", "admin@admin.com", "Manager", 99999, "090090090", "1.jpg"));
//        TimeStampDao dao = new TimeStampDao();
//        
//    }

}
