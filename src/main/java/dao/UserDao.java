/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author NonKirito
 */
public class UserDao implements DaoInterface<User>{

    @Override
    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO user (user_name, user_username, user_password, user_email, user_type, user_salary, user_tel, user_img)"
                    + "VALUES (?,?,?,?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getUsername());
            stmt.setString(3, object.getPassword());
            stmt.setString(4, object.getEmail());
            stmt.setString(5, object.getUserType());
            stmt.setDouble(6, object.getSalary());
            stmt.setString(7, object.getTel());
            stmt.setString(8, object.getImgPath());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT user_id, user_name, user_username, user_password, user_email, user_type, user_salary, user_tel, user_img FROM user;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("user_id");
                String name = result.getString("user_name");
                String userName = result.getString("user_username");
                String password = result.getString("user_password");
                String email = result.getString("user_email");
                String type = result.getString("user_type");
                double salary = result.getDouble("user_salary");
                String tel = result.getString("user_tel");
                String img = result.getString("user_img");
                User user = new User(id, userName, password, name, email, type, salary, tel, img);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT user_id, user_name, user_username, user_password, user_email, user_type, user_salary, user_tel, user_img FROM user WHERE user_id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int uid = result.getInt("user_id");
                String name = result.getString("user_name");
                String userName = result.getString("user_username");
                String password = result.getString("user_password");
                String email = result.getString("user_email");
                String type = result.getString("user_type");
                double salary = result.getDouble("user_salary");
                String tel = result.getString("user_tel");
                String img = result.getString("user_img");
                User user = new User(uid, userName, password, name, email, type, salary, tel, img);
                db.close();
                return user;
            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return null;
    }


    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM user WHERE user_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE user SET user_id = ?, user_name = ?, user_username = ?, user_password = ?, "
                    + "user_email = ?, user_type = ?, user_salary = ?, user_tel = ?, user_img = ? WHERE user_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getId());
            stmt.setString(2, object.getName());
            stmt.setString(3, object.getUsername());
            stmt.setString(4, object.getPassword());
            stmt.setString(5, object.getEmail());
            stmt.setString(6, object.getUserType());
            stmt.setDouble(7, object.getSalary());
            stmt.setString(8, object.getTel());
            stmt.setString(9, object.getImgPath());
            stmt.setInt(10, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return row;
    }
    public ArrayList<User> search(String nameUser) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM user  WHERE user_name LIKE  '%"+ nameUser +"%' " ;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("user_id");
                String name = result.getString("user_name");
                String userName = result.getString("user_username");
                String password = result.getString("user_password");
                String email = result.getString("user_email");
                String type = result.getString("user_type");
                double salary = result.getDouble("user_salary");
                String tel = result.getString("user_tel");
                String img = result.getString("user_img");
                User user = new User(id, userName, password, name, email, type, salary, tel, img);
                list.add(user);
                
            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return list;
    }
}
