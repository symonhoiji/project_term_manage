package dao;


import database.Database;
import java.sql.*;
import java.util.ArrayList;
import model.User;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Kanny
 */
public class LoginDao implements DaoInterface {
    
    Database db = Database.getInstance();
    Connection con = db.getConnection();
    
    
    @Override
    public int add(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList getAll() {
        ArrayList list = new ArrayList();
        try {
            String sql = "SELECT * FROM user";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("user_id");
                String username = result.getString("user_username");
                String password = result.getString("user_password");
                String name = result.getString("user_name");
                String email = result.getString("user_email");
                String user_type = result.getString("user_type");
                double salary = result.getDouble("user_salary");
                String tel = result.getString("user_tel");
                String img_path = result.getString("user_img");
                User u = new User(id, username, password, name, email, user_type, salary, tel, img_path);
                list.add(u);
            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Object get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int update(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void main(String[] args) {
        LoginDao dao = new LoginDao();
    }
}
