/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DetailOrder;
import model.Product;

/**
 *
 * @author admin
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO product (prod_name,prod_price,prod_img,prod_type,prod_sale,prod_remain) VALUES (?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setString(1,object.getName());
            stmt.setDouble(2,object.getPrice());
            stmt.setString(3,object.getImg());
            stmt.setString(4, object.getType());
            stmt.setInt(5, object.getSale());
            stmt.setInt(6, object.getRemaining());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            
            if(result.next()){
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }

         db.close();
         return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("prod_id");
                String name = result.getString("prod_name");
                double price = result.getDouble("prod_price");
                String img = result.getString("prod_img");
                String type = result.getString("prod_type");
                int  sale = result.getInt("prod_sale");
                int  remaining = result.getInt("prod_remain");
                Product product = new Product(id,name,price,img,type,sale,remaining);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        
        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
       Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name,price FROM product WHERE id =" +id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()){
                int pid = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                String img = result.getString("prod_img");
                String type = result.getString("prod_type");
                int  sale= result.getInt("prod_sale");
                int  remaining = result.getInt("prod_remain");
                Product product = new Product(id,name,price,img,type,sale,remaining);
                return product;
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return null;
    }
    
    public ArrayList<Product> getProductByType(String p_type,int page) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM product WHERE prod_type = \""+p_type+"\" LIMIT 6 OFFSET "+page+"";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("prod_id");
                String name = result.getString("prod_name");
                double price = result.getDouble("prod_price");
                String img = result.getString("prod_img");
                String type = result.getString("prod_type");
                int  sale = result.getInt("prod_sale");
                int  remaining = result.getInt("prod_remain");
                Product product = new Product(id,name,price,img,type,sale,remaining);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        
        db.close();
        return list;
    }
    public ArrayList<Product> getProductByType(String p_type) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM product WHERE prod_type = \""+p_type+"\"";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("prod_id");
                String name = result.getString("prod_name");
                double price = result.getDouble("prod_price");
                String img = result.getString("prod_img");
                String type = result.getString("prod_type");
                int  sale = result.getInt("prod_sale");
                int  remaining = result.getInt("prod_remain");
                Product product = new Product(id,name,price,img,type,sale,remaining);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        
        db.close();
        return list;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM product WHERE prod_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
           System.out.println(ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            //String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?;";
            String sql = "UPDATE product SET prod_name = ?,prod_price = ?"
            +",prod_img = ?,prod_type = ?,prod_remain = ? WHERE prod_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setDouble(2,object.getPrice());
            stmt.setString(3,object.getImg());
            stmt.setString(4,object.getType());
            stmt.setInt(5,object.getRemaining());
            stmt.setInt(6,object.getId());
            
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
       db.close();
       return row;
    }
    public int updateSales(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            //String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?;";
            String sql = "UPDATE product SET prod_remain = ?,prod_sale = ? WHERE prod_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,object.getRemaining() - object.getAmount());
            stmt.setInt(2,object.getSale() + object.getAmount());
            stmt.setInt(3,object.getId());

            row = stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
       return row;
    }
    
    
    public Product getBestSell() {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM product order by prod_sale desc";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            int id = result.getInt("prod_id");
            String name = result.getString("prod_name");
            double price = result.getDouble("prod_price");
            String img = result.getString("prod_img");
            String type = result.getString("prod_type");
            int  sale = result.getInt("prod_sale");
            int  remaining = result.getInt("prod_remain");
            Product product = new Product(id,name,price,img,type,sale,remaining);
            return product;
        } catch (SQLException ex) {
           System.out.println(ex);
        }
        db.close();
        return null;
    }
    
    public Product getWorseSell() {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM product order by prod_sale";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            int id = result.getInt("prod_id");
            String name = result.getString("prod_name");
            double price = result.getDouble("prod_price");
            String img = result.getString("prod_img");
            String type = result.getString("prod_type");
            int  sale = result.getInt("prod_sale");
            int  remaining = result.getInt("prod_remain");
            Product product = new Product(id,name,price,img,type,sale,remaining);
            return product;
        } catch (SQLException ex) {
           System.out.println(ex);
        }
        db.close();
        return null;
    }
    
    
    public ArrayList<Product> Search(String NameProduct) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM product  WHERE prod_name LIKE  '%" + NameProduct + "%' ";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("prod_id");
                String name = result.getString("prod_name");
                double price = result.getDouble("prod_price");
                String img = result.getString("prod_img");
                String type = result.getString("prod_type");
                int sale = result.getInt("prod_sale");
                int  remaining = result.getInt("prod_remain");
                Product product = new Product(id,name,price,img,type,sale,remaining);
                list.add(product);

            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return list;
    }
    
    public ArrayList<Product> getAllByRemain() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM product order by prod_remain DESC ";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("prod_id");
                String name = result.getString("prod_name");
                double price = result.getDouble("prod_price");
                String img = result.getString("prod_img");
                String type = result.getString("prod_type");
                int  sale = result.getInt("prod_sale");
                int  remaining = result.getInt("prod_remain");
                Product product = new Product(id,name,price,img,type,sale,remaining);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        
        db.close();
        return list;
    }

    
//     public static void main(String args[]) {
//         Product test = new Product(1,"Coco",39.0,"cocoa.jpg","Drink",15,0);
//         ProductDao tester = new ProductDao();
//         System.out.println(tester.delete(1));
//         
//     }
}