/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DetailOrder;
import model.Member;
import model.Order;
import model.Product;
import model.Promotion;
import model.User;

/**
 *
 * @author james
 */
public class OrderDao implements DaoInterface<Order> {

    @Override
    public int add(Order object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO order_All ( mem_id, user_id,prom_id, or_price_total ) VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getMember().getId());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setInt(3, object.getPromotion().getId());
            stmt.setDouble(4, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (DetailOrder r : object.getDetailorder()) {

                ProductDao productdao = new ProductDao();
                productdao.updateSales(r.getProduct());
                
                String sqlDetail = "INSERT INTO detail_order ( or_id, prod_id, or_de_qty) VALUES (?,?,? )";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getOrder().getId());
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setInt(3, r.getAmount());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmtDetail.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                }
                
            }

        } catch (SQLException ex) {
            System.out.println("Error: to create receipt");
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Order> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM order_All o,member m,user u ,promotion p "
                    + "WHERE o.mem_id = m.mem_id AND o.user_id = u.user_id AND o.prom_id = p.prom_id ORDER BY or_de_date DESC";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("or_id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("or_de_date"));
                double orderTotal = result.getDouble("or_price_total");
                
                int memberId = result.getInt("mem_id");
                String memberName = result.getString("mem_name");
                String memberTel = result.getString("mem_tel");
                
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userUsername = result.getString("user_username");
                String userPassword = result.getString("user_password");
                String userEmail = result.getString("user_email");
                String userType = result.getString("user_type");
                double userSalary = result.getDouble("user_salary");
                String userTel = result.getString("user_tel");
                String userImg = result.getString("user_img");
                
                int promId = result.getInt("prom_id");
                int promPersent = result.getInt("prom_dis_per");
                String promType = result.getString("prom_dis_type");
                String promDesc = result.getString("prom_desc");
                String promStatus = result.getString("prom_status");
                
                
                Order order = new Order(id, created,orderTotal,
                        new User(userId, userUsername, userPassword, userName, userEmail, userType, userSalary, userTel, userImg),
                        new Member(memberId, memberName, memberTel),
                        new Promotion(promId, promPersent, promType, promDesc, promStatus));
                list.add(order);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all receipt + "+ex);
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all receipt + "+ex);
        }

        db.close();
        return list;
    }

    @Override
    public Order get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * "
                    + "FROM order_All o,member m,user u ,promotion p "
                    + "WHERE o.or_id = ? AND o.mem_id = m.mem_id AND o.user_id = u.user_id AND o.prom_id = p.prom_id ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int oid = result.getInt("or_id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("or_de_date"));
                double orderTotal = result.getDouble("or_price_total");
                        
                int memberId = result.getInt("mem_id");
                String memberName = result.getString("mem_name");
                String memberTel = result.getString("mem_tel");
                
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userUsername = result.getString("user_username");
                String userPassword = result.getString("user_password");
                String userEmail = result.getString("user_email");
                String userType = result.getString("user_type");
                double userSalary = result.getDouble("user_salary");
                String userTel = result.getString("user_tel");
                String userImg = result.getString("user_img");
                
                int promId = result.getInt("prom_id");
                int promPersent = result.getInt("prom_dis_per");
                String promType = result.getString("prom_dis_type");
                String promDesc = result.getString("prom_desc");
                String promStatus = result.getString("prom_status");
                
                
                Order order = new Order(oid, created,orderTotal,
                        new User(userId, userUsername, userPassword, userName, userEmail, userType, userSalary, userTel, userImg),
                        new Member(memberId, memberName, memberTel),
                        new Promotion(promId, promPersent, promType, promDesc, promStatus));

                getDetailOrder(conn, id, order);
                
                return order;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id " + id + " !!"+ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all receipt" + ex.getMessage());
        }

        return null;
    }

    private void getDetailOrder(Connection conn, int id, Order order) throws SQLException {
        String sqlDetail = "SELECT * "
                + "FROM detail_order do,product p "
                + "WHERE or_id = ? AND do.prod_id = p.prod_id;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        
        while (resultDetail.next()) {
            int receiptId = resultDetail.getInt("or_de_id");
            int productId = resultDetail.getInt("prod_id");
            String productName = resultDetail.getString("prod_name");
            double productPrice = resultDetail.getDouble("prod_price");
            String productImg = resultDetail.getString("prod_img");
            String productType = resultDetail.getString("prod_type");
            int productSale = resultDetail.getInt("prod_sale");
            double price = resultDetail.getDouble("prod_price");
            int amount = resultDetail.getInt("or_de_qty");
            Product product = new Product(productId, productName, productPrice,productImg,productType,productSale);
            order.addDetailOrder(receiptId,product, amount,price);
        }
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM order_All WHERE or_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete receipt id " + id + " !!");
        }

        db.close();
        return row;
    }

    @Override
    public int update(Order object) {
//        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        int row = 0;
//        try {
//            String sql = "UPDATE receipt SET name = ?, price =? WHERE id = ?";
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, object.getName());
//            stmt.setDouble(2, object.getPrice());
//            stmt.setInt(3, object.getId());
//            row = stmt.executeUpdate();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(TestReceipt.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        db.close();
        return 0;
    }

    public static void main(String[] args) {
//        Product p1 = new Product(1,"Cocoa",40,"cocoa.jpg","Drink",15);
//        Product p2 = new Product(2,"Hot Latte",45,"cocoa.jpg","Drink",15);
//        User seller = new User(1, "admin", "123", "Admin", "admin@admin.com", "Manager", 99999, "090090090", "1.jpg");
//        Member member = new Member(2, "Marcus Rashford", "0123456788");
//        Promotion prom = new Promotion(1, 1, "Yes", "Discount for member 1 percent", "Using");
//        Order order = new Order(0,seller, member, prom);
//        order.addDetailOrder(p1, 1);
//        order.addDetailOrder(p2, 3);
//        System.out.println(order);
//        OrderDao dao = new OrderDao();
//        System.out.println("id =" + dao.add(order));
//        System.out.println("Recept after add: " + order);
//        System.out.println("Get all" + dao.getAll());
//        
//        Order newReceipt = dao.get(order.getId());
//        System.out.println("New Order :" + newReceipt);
    }

    public ArrayList<Order> Search(String text) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM order_All o,member m,user u,promotion p  "
                    + "WHERE o.mem_id = m.mem_id AND o.user_id = u.user_id AND o.prom_id = p.prom_id AND u.user_name LIKE  '%"+ text +"%' ORDER BY or_de_date DESC" ;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("or_id");
                Date created = null;
                try {
                    created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("or_de_date"));
                } catch (ParseException ex) {
                    Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
                }
                double orderTotal = result.getDouble("or_price_total");
                
                int memberId = result.getInt("mem_id");
                String memberName = result.getString("mem_name");
                String memberTel = result.getString("mem_tel");
                
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userUsername = result.getString("user_username");
                String userPassword = result.getString("user_password");
                String userEmail = result.getString("user_email");
                String userType = result.getString("user_type");
                double userSalary = result.getDouble("user_salary");
                String userTel = result.getString("user_tel");
                String userImg = result.getString("user_img");
                
                int promId = result.getInt("prom_id");
                int promPersent = result.getInt("prom_dis_per");
                String promType = result.getString("prom_dis_type");
                String promDesc = result.getString("prom_desc");
                String promStatus = result.getString("prom_status");
                
                
                Order order = new Order(id, created,orderTotal,
                        new User(userId, userUsername, userPassword, userName, userEmail, userType, userSalary, userTel, userImg),
                        new Member(memberId, memberName, memberTel),
                        new Promotion(promId, promPersent, promType, promDesc, promStatus));
                
                
                list.add(order);
                
            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return list;
    }

}
