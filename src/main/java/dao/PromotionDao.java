/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Promotion;

public class PromotionDao implements DaoInterface<Promotion> {

    public int add(Promotion object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO promotion (prom_dis_per, "
                    + "prom_dis_type,"
                    + "prom_desc,"
                    + "prom_status)"
                    + "VALUES (?,?,?,?);";

            PreparedStatement stmt = conn.prepareStatement(sql);
            System.out.println(stmt.getClass().toString());
            stmt.setInt(1, object.getDiscount());
            stmt.setString(2, object.getType());
            stmt.setString(3, object.getDesc());
            stmt.setString(4, object.getStatus());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Promotion> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT prom_id,"
                    + "prom_dis_per,"
                    + "prom_dis_type,"
                    + "prom_desc,"
                    + "prom_status "
                    + "FROM promotion;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("prom_id");
                int persent = result.getInt("prom_dis_per");
                String type = result.getString("prom_dis_type");
                String desc = result.getString("prom_desc");
                String status = result.getString("prom_status");
                Promotion promotion = new Promotion(id, persent, type, desc, status);
                list.add(promotion);
            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Promotion get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT prom_id,"
                    + "prom_dis_per,"
                    + "prom_dis_type,"
                    + "prom_desc,"
                    + "prom_status "
                    + "FROM promotion WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("prom_id");
                int persent = result.getInt("prom_dis_per");
                String type = result.getString("prom_dis_type");
                String desc = result.getString("prom_desc");
                String status = result.getString("prom_status");
                Promotion promotion = new Promotion(pid, persent, type, desc, status);
                db.close();
                return promotion;
            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM promotion WHERE prom_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(Promotion object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE promotion SET "
                    + "prom_id = ?,"
                    + "prom_dis_per = ?,"
                    + "prom_dis_type = ?,"
                    + "prom_desc = ?,"
                    + "prom_status = ? "
                    + "WHERE prom_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getId());
            stmt.setInt(2, object.getDiscount());
            stmt.setString(3, object.getType());
            stmt.setString(4, object.getDesc());
            stmt.setString(5, object.getStatus());
            stmt.setInt(6, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return row;
    }

    public ArrayList<Promotion> Search(int disPercent) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM promotion  WHERE prom_dis_per LIKE  '%" + disPercent + "%' ";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int poid = result.getInt("prom_id");
                int popersent = result.getInt("prom_dis_per");
                String potype = result.getString("prom_dis_type");
                String podesc = result.getString("prom_desc");
                String postatus = result.getString("prom_status");
                Promotion promotion = new Promotion(poid, popersent, potype, podesc, postatus);
                list.add(promotion);

            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        db.close();
        return list;
    }

}
