package model;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String img;
    private String type;
    private int sale;
    private int amount;
    private int Remaining;

    public Product(int id, String name, double price,String img,String type,int sale,int Remaining) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.img = img;
        this.type = type;
        this.sale = sale;
        this.amount = 1;
        this.Remaining = Remaining;
    }
    
    public Product(int id, String name, double price,String img,String type,int sale) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.img = img;
        this.type = type;
        this.sale = sale;
        this.amount = 1;
        this.Remaining = Remaining;
    }
    
    public Product(int id, String name, double price,String img,String type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.img = img;
        this.type = type;
        this.amount = 1;
    }

    public Product(int id, String name, double price, String img, String type, int sale, int amount,int Remaining) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.img = img;
        this.type = type;
        this.sale = sale;
        this.amount = amount;
        this.Remaining = Remaining;
    }

    public int getRemaining() {
        return Remaining;
    }

    public void setRemaining(int Remaining) {
        this.Remaining = Remaining;
    }
    

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    public int addAmount(){
        amount++;
        return amount;
    }
    
    public int getTotal(){
        return (int) (price*amount);
    }
    public int getSale() {
        return sale;
    }

    public void setSale(int sale) {
        this.sale = sale;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", img=" + img + ", type=" + type + ", sale=" + sale + ", amount=" + amount + ", Remaining=" + Remaining + '}';
    }
}
