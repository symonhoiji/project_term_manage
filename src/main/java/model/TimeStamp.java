/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Mr.Omega
 */
public class TimeStamp {
    private int id;
    private String signIn;
    private String signOut;
    private String date;
    private User user;
    
    public TimeStamp(int id, String signIn, String signOut, String date, User user) {
        this.id = id;
        this.signIn = signIn;
        this.signOut = signOut;
        this.date = date;
        this.user = user;
    }

    public TimeStamp(User user) {
        this(-1,null,null,null,user);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSignIn() {
        String[] temp=signIn.split(":");
        int hour=Integer.parseInt(temp[0])+7;
        return hour+":"+temp[1]+":"+temp[2];
    }

    public void setSignIn(String signIn) {
        this.signIn = signIn;
    }

    public String getSignOut() {
        if(signOut!=null){
             String[] temp=signOut.split(":");
            int hour=Integer.parseInt(temp[0])+7;
            System.out.println(temp.length);
            return hour+":"+temp[1]+":"+temp[2];
        }else 
            return  signOut;
       
    }

    public void setSignOut(String signOut) {
        this.signOut = signOut;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDate() {
//        String[] ary = date.toString().split(" ");
//        return ary[0]+" "+ary[2]+" "+ary[1];
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    @Override
    public String toString() {
        return "TimeStamp{" + "id=" + id + ", signIn=" + signIn + ", signOut=" + signOut + ", date=" + date + ", userId=" + user.getId() + '}';
    }
    
    
    
}
