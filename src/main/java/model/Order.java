/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author james
 */
public class Order {

    private int id;
    private Date created;
    private double total;
    private User seller;
    private Member member;
    private Promotion promotion;
    private ArrayList<DetailOrder> detailorder;

    public Order(int id,Date created,double total, User seller, Member member, Promotion promotion) {
        this.id = id;
        this.created = created;
        this.total = total;
        this.seller = seller;
        this.member = member;
        this.promotion = promotion;
        this.detailorder = new ArrayList<>();
    }
    
    public Order( double total,User seller, Member member, Promotion promotion){
        this(-1,null,total,seller,member,promotion);
    }
    
    public  void addDetailOrder(int id,Product product,int amount,double price){
        for (int row = 0; row <detailorder.size(); row++) {
            DetailOrder d = detailorder.get(row);
            if(d.getProduct().getId()== product.getId()){
                d.addAmount(amount); 
                return;
            }
            
        }
        detailorder.add(new DetailOrder(id,product,amount,price,this));
    }
    

    public  void addDetailOrder(Product product,int amount ){
        addDetailOrder(-1, product, amount,product.getPrice());
    }
    
    public void setTotal(double total){
        this.total = total;
    }
    //Override Method
    public  void addDetailOrder(int id,Product product){
        detailorder.add(new DetailOrder(id,product,product.getAmount(),product.getPrice(),this));
    }
    //Override Method
    public  void addDetailOrder(Product product){
        addDetailOrder(-1, product);
    }
    
    
    public void deleteDetailOrder(int row){
        detailorder.remove(row);
    }
    public double getTotal() {
//        this.total = 0;
//        for (DetailOrder r : detailorder) {
//            total = total + r.getTotal();
//        }
        return total;
    }
    
    public double getTotalAll() {
        this.total = 0;
        for (DetailOrder r : detailorder) {
            total = total + r.getTotal();
        }
        return total;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public ArrayList<DetailOrder> getDetailorder() {
        return detailorder;
    }

    public void setDetailorder(ArrayList<DetailOrder> detailorder) {
        this.detailorder = detailorder;
    }

    @Override
    public String toString() {
        String str = "Receipt{" + "id=" + id 
                + ", created=" + created 
                + ", seller=" + seller 
                + ", customer=" + member
                + ", promotion=" + promotion
                + ", total="+total
                + "}\n";
        for (DetailOrder r : detailorder) {
            str += r.toString() + "\n";
        }
        return str;
    }

}
