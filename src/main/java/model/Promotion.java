/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


public class Promotion {
    private int id;
    private int discount;
    private String type;
    private String desc;
    private String status;

    public Promotion(int id, int discount, String type, String desc, String status) {
        this.id = id;
        this.discount = discount;
        this.type = type;
        this.desc = desc;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", discount=" + discount + ", type=" + type + ", desc=" + desc + ", status=" + status + '}';
    }
    
}
