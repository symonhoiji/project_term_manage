/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kanny
 */
public class Database {

    private static Database instance = new Database();
    private Connection con;

    private Database() {
    }

    public static Database getInstance() {
        String dbPath = "./db/shopdb.db";
        try {
            if (instance.con == null || instance.con.isClosed()) {

                Class.forName("org.sqlite.JDBC");
                instance.con = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Connection accepted");
            }
        } catch (Exception e) {
            System.out.println("Can not connect to the db" + e);
            System.exit(0);
        }

        return instance;
    }

    public static void close() {
        try {
            if (instance.con != null && !instance.con.isClosed()) {
                instance.con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.con = null;
    }

    public Connection getConnection() {
        return instance.con;
    }
}
