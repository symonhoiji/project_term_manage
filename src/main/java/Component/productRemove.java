/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *///
package Component;

import dao.ProductDao;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import model.Product;
import ui.ProductPanel;

/**
 *
 * @author Symon
 */
public class productRemove extends javax.swing.JFrame {
    String imagePath = "";
    /**
     * Creates new form memberAdd
     */
    private ArrayList<Product> productList;
    private ProductDao dao = new ProductDao();
    ProductPanel tblProduct;
    int remove;
    int ProductId,Sale;
    
    public productRemove() {
        initComponents();
    }
    
    public productRemove(Product removeProduct,ProductPanel panel) {
        initComponents();
        this.tblProduct = panel;
        remove = removeProduct.getId();
        System.out.println(remove);
        lblId.setText("" + removeProduct.getId());
        lblName.setText(removeProduct.getName());
    }
    
    
    public void refreshTable() {
        ProductDao dao = new ProductDao();
        ArrayList<Product> newList = dao.getAll();
        productList = newList;
        tblProduct.refreshTable();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        showPanel = new javax.swing.JPanel();
        headerPanel = new javax.swing.JPanel();
        lblHeader1 = new javax.swing.JLabel();
        okBtn = new javax.swing.JButton();
        cancelBtn = new javax.swing.JButton();
        lblIDH = new javax.swing.JLabel();
        lblRemove = new javax.swing.JLabel();
        lblNameH = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        lblId = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Remove Product");
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(455, 364));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        showPanel.setBackground(new java.awt.Color(255, 255, 255));
        showPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        headerPanel.setBackground(new java.awt.Color(153, 51, 0));
        headerPanel.setForeground(new java.awt.Color(153, 51, 0));
        headerPanel.setMinimumSize(new java.awt.Dimension(455, 364));

        lblHeader1.setBackground(new java.awt.Color(255, 255, 255));
        lblHeader1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblHeader1.setForeground(new java.awt.Color(255, 255, 255));
        lblHeader1.setText("Remove Product");

        javax.swing.GroupLayout headerPanelLayout = new javax.swing.GroupLayout(headerPanel);
        headerPanel.setLayout(headerPanelLayout);
        headerPanelLayout.setHorizontalGroup(
            headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerPanelLayout.createSequentialGroup()
                .addGap(141, 141, 141)
                .addComponent(lblHeader1)
                .addContainerGap(121, Short.MAX_VALUE))
        );
        headerPanelLayout.setVerticalGroup(
            headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerPanelLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(lblHeader1)
                .addContainerGap(304, Short.MAX_VALUE))
        );

        showPanel.add(headerPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 460, 90));

        okBtn.setBackground(new java.awt.Color(0, 153, 51));
        okBtn.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        okBtn.setForeground(new java.awt.Color(255, 255, 255));
        okBtn.setText("Ok");
        okBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okBtnActionPerformed(evt);
            }
        });
        showPanel.add(okBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 280, 110, 40));

        cancelBtn.setBackground(new java.awt.Color(204, 0, 0));
        cancelBtn.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cancelBtn.setForeground(new java.awt.Color(255, 255, 255));
        cancelBtn.setText("Cancel");
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });
        showPanel.add(cancelBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 280, 120, 40));

        lblIDH.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblIDH.setText("ID:");
        showPanel.add(lblIDH, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 180, 30, 30));

        lblRemove.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblRemove.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblRemove.setText("Confirm to remove?");
        showPanel.add(lblRemove, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 120, 270, 50));

        lblNameH.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblNameH.setLabelFor(lblName);
        lblNameH.setText("Name:");
        showPanel.add(lblNameH, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 230, 60, -1));

        lblName.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        showPanel.add(lblName, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 220, 130, 40));

        lblId.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        showPanel.add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 170, 130, 50));

        getContentPane().add(showPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 460, 370));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void okBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okBtnActionPerformed
        
        dao.delete(remove);
        
        JFrame frame = new JFrame("JOptionPane showMessageAdd");
        JOptionPane.showMessageDialog(frame,
                "Remove Successful.",
                "Notification",
                JOptionPane.OK_OPTION);
        dispose();
        tblProduct.refreshTable();   
//        this.dispose();
    }//GEN-LAST:event_okBtnActionPerformed

    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelBtnActionPerformed
     /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(productRemove.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(productRemove.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(productRemove.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(productRemove.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new productRemove().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelBtn;
    private javax.swing.JPanel headerPanel;
    private javax.swing.JLabel lblHeader1;
    private javax.swing.JLabel lblIDH;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblNameH;
    private javax.swing.JLabel lblRemove;
    private javax.swing.JButton okBtn;
    private javax.swing.JPanel showPanel;
    // End of variables declaration//GEN-END:variables
}
