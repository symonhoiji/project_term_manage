Layout Resolution 1280x720 **




================ Java Version =================
    - JDK 15 
        https://www.oracle.com/java/technologies/javase-jdk15-downloads.html
    - Apache NetBeans IDE 12.0 **
        https://netbeans.apache.org/download/nb120/nb120.html
        Download : https://www.apache.org/dyn/closer.cgi/netbeans/netbeans/12.0/Apache-NetBeans-12.0-bin-windows-x64.exe

    - UI at Lucidchart
        https://lucid.app/invitations/accept/72f43ce8-d772-4a99-9bc0-67272e3ff16a